//
//  APIController.swift
//  MusicPlayer
//
//  Created by Jameson Quave on 9/16/14.
//  Copyright (c) 2014 JQ Software LLC. All rights reserved.
//

import Foundation

let dropboxURL = "https://dl.dropboxusercontent.com/u/746330/facts.json"

protocol APIControllerProtocol {
    func didReceiveAPIResults(results: NSDictionary)
}

class APIController {
    
    var delegate: APIControllerProtocol

    init(delegate: APIControllerProtocol) {
        self.delegate = delegate
    }
    
    func retrieveFacts(path: String) {
        let url = NSURL(string: path)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url!, completionHandler: {data, response, error -> Void in
            println("Task completed")
            if(error != nil) {
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            var err: NSError?
            
            var convertedData = self.convertDataToNSASCII(data);
            
            var jsonResult = NSJSONSerialization.JSONObjectWithData(convertedData, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSDictionary
            
            if(err != nil) {
                // If there is an error parsing JSON, print it to the console
                println("JSON Error \(err!.localizedDescription)")
            }
            
            let results: NSArray = jsonResult["rows"] as NSArray
            
            self.delegate.didReceiveAPIResults(jsonResult) 
        })
        task.resume()
    }
    
    // function to convert data from
    func convertDataToNSASCII(data: NSData) -> NSData {
        var NSASCIIString: NSString = NSString(data: data, encoding: NSASCIIStringEncoding)!
        
        return NSASCIIString.dataUsingEncoding(NSUTF8StringEncoding)!
    }
}