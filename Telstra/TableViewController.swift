//
//  TableViewController.swift
//  TableViewCellWithAutoLayout
//
//  Copyright (c) 2014 Tyler Fox
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,
    APIControllerProtocol
{
    var tableView : UITableView = UITableView()

    let kCellIdentifier = "CellIdentifier"
    
    var facts = [Fact]()
    var imageCache = [String : UIImage]()
    var api : APIController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // create new api object and set delegate to self
        api = APIController(delegate: self)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        api!.retrieveFacts(dropboxURL)
        
        title = "Telstra"
        
        // add observers
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "titleChanged:", name:"titleChangedNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onContentSizeChange:", name: UIContentSizeCategoryDidChangeNotification, object: nil)
        
        // add refresh button
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: "Refresh")
        
        tableView.setTranslatesAutoresizingMaskIntoConstraints(false)
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(TableViewCell.self, forCellReuseIdentifier: kCellIdentifier)
        // set this to whatever your "average" cell height is; it doesn't need to be very accurate
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        view.addSubview(tableView)
        
        let viewsDictionary = ["tableView":tableView]
        
        let tableView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        let tableView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewsDictionary)
        
        view.addConstraints(tableView_constraint_H)
        view.addConstraints(tableView_constraint_V)
    }
    
    func titleChanged(notification: NSNotification){
        if let userInfo:Dictionary<String, String!> = notification.userInfo as? Dictionary<String,String!>
        {
            title = userInfo["title"]
            println("Title changed to : \(title)")
        }
    }
    
    override func viewDidDisappear(animated: Bool)  {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func onContentSizeChange(notification: NSNotification) {
        tableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return facts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        // This will be the case for programmatically loaded cells (see viewDidLoad to switch approaches)
        let cell: TableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as TableViewCell
        let fact = facts[indexPath.row]
        
        cell.titleLabel.text = fact.title
        cell.descriptionLabel.text = fact.description
        cell.thumbnailImageView.image = UIImage(named: "Blank52")

        // Grab the imageHref key to get an image URL for the app's thumbnail
        var urlString = fact.imageHref
        
        // Check our image cache for the existing key. This is just a dictionary of UIImages
        var image = self.imageCache[urlString]
        
        if( image == nil ) {
            // If the image does not exist, we need to download it, and check we have an image url
            if urlString.rangeOfString("nil") == nil{
                var imgURL: NSURL = NSURL(string: urlString)!
                
                // Download an NSData representation of the image at the URL
                let request: NSURLRequest = NSURLRequest(URL: imgURL)
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
                    if error == nil {
                        image = UIImage(data: data)
                        
                        // Store the image in to our cache
                        self.imageCache[urlString] = image
                        dispatch_async(dispatch_get_main_queue(), {
                            if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableViewCell {
                                cellToUpdate.thumbnailImageView.image = image
                            }
                        })
                    }
                    else {
                        println("Error: \(error.localizedDescription)")
                    }
                })
            }
        }
        else {
            // load image from cache
            dispatch_async(dispatch_get_main_queue(), {
                if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) as? TableViewCell{
                    cellToUpdate.thumbnailImageView.image = image
                }
            })
        }
        
        cell.makeLayout()

        return cell
    }
    
    // refresh function
    func Refresh()
    {
        // refresh button removes table and fires another request for facts
        facts.removeAll()
        self.tableView.reloadData()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        api!.retrieveFacts(dropboxURL)
    }
    
    func didReceiveAPIResults(results: NSDictionary) {
        let userInfo:Dictionary<String, String!> = ["title":results["title"] as String]
        // from deserialized json response deserialize objects into array and reload table
        NSNotificationCenter.defaultCenter().postNotificationName("titleChangedNotification", object:userInfo)
        
        var resultsArr: NSArray = results["rows"] as NSArray
        dispatch_async(dispatch_get_main_queue(), {
            self.facts = Fact.FactsWithJSON(resultsArr)
            self.tableView.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })
    }
}
