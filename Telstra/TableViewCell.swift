//
//  CustomCell.swift
//  Telstra
//
//  Created by Michael Williams on 8/11/2014.
//  Copyright (c) 2014 Michael Williams. All rights reserved.
//

import Foundation
import UIKit

class TableViewCell: UITableViewCell
{    
    var titleLabel: UILabel = UILabel()
    var descriptionLabel: UILabel = UILabel()
    var thumbnailImageView: UIImageView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        // override init functions
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    func makeLayout()
    {
        // title label, set translation to false to allow user-defined constraints
        titleLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
        titleLabel.numberOfLines = 0

        self.contentView.addSubview(titleLabel)

        descriptionLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        descriptionLabel.font = UIFont.systemFontOfSize(14.0)
        descriptionLabel.numberOfLines = 0

        self.contentView.addSubview(descriptionLabel)

        thumbnailImageView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.contentView.addSubview(thumbnailImageView)
        
        // create views dictionary for layout constraints
        let viewsDictionary = [ "title" : titleLabel, "body" : descriptionLabel, "image" : thumbnailImageView ]

        // metrics dictionary for UI sizing
        let metricsDictionary = [ "imageDim" : 100]

        var const1 = NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[image(imageDim)]-5-[title]-5-|", options: NSLayoutFormatOptions(0), metrics: metricsDictionary, views: viewsDictionary)
        contentView.addConstraints(const1)
        
        var const2 = NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[image(imageDim)]-5-[body]-5-|", options:NSLayoutFormatOptions(0), metrics: metricsDictionary, views: viewsDictionary)
        contentView.addConstraints(const2)

        var const3 = NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[title]-5-[body]-(>=5)-|", options:NSLayoutFormatOptions(0), metrics: metricsDictionary, views: viewsDictionary)
        contentView.addConstraints(const3)

        var const4 = NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[image(imageDim)]-(>=5)-|", options:NSLayoutFormatOptions(0), metrics: metricsDictionary, views: viewsDictionary)
        contentView.addConstraints(const4)
    }
}
