//
//  Album.swift
//  MusicPlayer
//
//  Created by Jameson Quave on 9/16/14.
//  Copyright (c) 2014 JQ Software LLC. All rights reserved.
//

import Foundation

class Fact {
    var title: String
    var description: String
    var imageHref: String
    
    init(title: String, description: String, imageHref: String)  {
        self.title = title
        self.description = description
        self.imageHref = imageHref
    }
    
    class func FactsWithJSON(allResults: NSArray) -> [Fact] {
        
        // Create an empty array of Albums to append to from this list
        var facts = [Fact]()
        
        // Store the results in our table data array
        if allResults.count>0 {
            
            // Sometimes iTunes returns a collection, not a track, so we check both for the 'name'
            for result in allResults {
                
                var newTitle = result["title"] as? String
                
                if newTitle == nil {
                    newTitle = "No Title"
                }
                
                var newDescription = result["description"] as? String
                
                if newDescription == nil {
                    newDescription = "No Description"
                }
                
                var newImageHref = result["imageHref"] as? String
                
                if newImageHref == nil {
                    newImageHref = "nil"
                }
                
               // println("New Fact - Title: \(newTitle!) Description: \(newDescription!) ImageHref: \(newImageHref!)")
                
                var newFact = Fact(title: newTitle!, description: newDescription!, imageHref: newImageHref!)
   
                facts.append(newFact)

            }
        }
        
        return facts
    }
    
}